from flask_wtf import FlaskForm
from wtforms import SelectField, StringField, SubmitField
from wtforms.validators import DataRequired, Length


class SearchForm(FlaskForm):
    search_term = StringField(
        '', [Length(min=1, max=50), DataRequired()])
    search_specify = SelectField(
        '', choices=[('q', 'Query'), ('author', 'Author'), ('title', 'Title')])
    submit = SubmitField('Search')
