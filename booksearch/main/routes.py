from flask import Blueprint, redirect, render_template, session, url_for

from booksearch.main.forms import SearchForm
from booksearch.main.utils import get_book_info, get_books_info

main = Blueprint('main', __name__)


@main.route("/")
def home():
    form = SearchForm()
    if form.validate_on_submit():
        session['search_term'] = form.search_term.data
        session['search_specify'] = form.search_specify.data
        return redirect(url_for('results', search=session['search_term'], specify=session['search_specify']))
    return render_template('home.html', form=form)


@main.route("/about")
def about():
    return render_template('about.html', title='About')
