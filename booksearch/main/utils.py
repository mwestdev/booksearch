import json

import requests


def get_books_info(search_term, search_specify):
    api_url_base = 'http://openlibrary.org/search.json?'
    headers = {'Content-Type': 'application/json'}
    api_url = '{0}{1}={2}'.format(api_url_base, search_specify, search_term)

    response = requests.get(api_url, headers=headers)

    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return None


def get_book_info(edition_key):
    api_url_base = 'https://openlibrary.org/api/books?bibkeys=OLID:'
    api_url_head = '&format=json&jscmd=details'
    headers = {'Content-Type': 'application/json'}
    OLID = edition_key
    api_url = '{0}{1}{2}'.format(api_url_base, OLID, api_url_head)

    response = requests.get(api_url, headers=headers)

    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return None
